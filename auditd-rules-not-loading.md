# 2020/05/18

## Making auditd rules work

So, I have been trying to finalize configuration of CIS Docker Benchmark on nodes, so that all the controls are applied. While applying the last 3 controls, I found that when working on SLES 12 SP2 nodes, I was unable to install `auditd` or configure it as I was doing on Ubuntu (obviously).

Originally I applied the auditd controls by following these steps:

```bash
apt-get install auditd
vim /etc/audit/audit.rules
```
adding these lines to the file:
```
-w /usr/bin/dockerd -k docker
-w /var/lib/docker -k docker
-w /etc/docker -k docker
-w /lib/systemd/system/docker.service -k docker
-w /lib/systemd/system/docker.socket -k docker
-w /etc/default/docker -k docker
-w /usr/bin/containerd -k docker
-w /etc/docker/daemon.json -k docker
```
and restarting the service: `service auditd restart`

After doing that, if I ran `auditctl -l`, the docker rules would show up.

But in SLES, we have a different story, as usual.

So, after rebuilding one of my Kubevirt VMs with a SLES 12 SP2 image, I went ahead applying the CIS controls again and found my `/etc/audit/audit.rules` file looks full of random rules I never added... Even more, if I added my Docker audit rules at the end of the file and restarted the service, my rules wouldn't even show up upon `auditctl -l`. So, what is the difference here? What's happening?

First, I found my rules were not valid for the SLES Docker installation. The rules I needed to add were actually these:
```bash
-w /usr/bin/docker -k docker
-w /var/lib/docker -k docker
-w /etc/docker -k docker
-w /usr/lib/systemd/system/docker.service -k docker
-w /var/run/docker.sock -k docker
-w /etc/sysconfig/docker -k docker
-w /usr/bin/dockerd -k docker
-w /etc/docker/daemon.json -k docker
```

Then, I saw no changes in the `/etc/audit/audit.rules` file were being read by the service, even after doing a restart. After looking into some research, I came to several realizations:
   * Maybe there was a formatting error in my `/etc/audit/audit.rules` file
   * You can also generate that file based on the contents of .rules files in the `/etc/audit/rules.d` directory
   * My SLES node actually has already 2 `rules.d` files: `audit.rules` and `docker.rules`.
   * Most of the audit rules I want to apply are already on the `docker.rules` file, but still not being loaded

I found out that you can regenerate the `audit.rules` file with the `augenrules` command, which did not work for me. Then I read somewhere that `.rules` files need to end in a newline, so that the audit service is able to read them correctly. 

This prompted me to:
1. Add a newline at the end of my `docker.rules` file
2. Perform a `service auditd restart`, which apparently also runs the `augenrules` command.

By doing that, I was able to load the `docker.rules` contents and start auditing them:
```
-w /usr/bin/docker -p rwxa -k docker
-w /var/lib/docker/ -p rwxa -k docker
-w /etc/docker/ -p rwxa -k docker
-w /usr/lib/systemd/system/docker-registry.service -p rwxa -k docker
-w /usr/lib/systemd/system/docker.service -p rwxa -k docker
-w /var/run/docker.sock -p rwxa -k docker
-w /etc/sysconfig/docker -p rwxa -k docker
-w /etc/sysconfig/docker-network -p rwxa -k docker
-w /etc/sysconfig/docker-registry -p rwxa -k docker
-w /etc/sysconfig/docker-storage -p rwxa -k docker
-w /etc/default/docker -p rwxa -k docker
```

Just to be safe, I went ahead and added a couple of rules more to my `docker.rules` that were on my original list, but not on the file provided on my SLES image, did the service restart again and voila!
```bash
node:~ # service auditd restart
node:~ # auditctl -l
-w /usr/bin/docker -p rwxa -k docker
-w /var/lib/docker/ -p rwxa -k docker
-w /etc/docker/ -p rwxa -k docker
-w /usr/lib/systemd/system/docker-registry.service -p rwxa -k docker
-w /usr/lib/systemd/system/docker.service -p rwxa -k docker
-w /var/run/docker.sock -p rwxa -k docker
-w /etc/sysconfig/docker -p rwxa -k docker
-w /etc/sysconfig/docker-network -p rwxa -k docker
-w /etc/sysconfig/docker-registry -p rwxa -k docker
-w /etc/sysconfig/docker-storage -p rwxa -k docker
-w /etc/default/docker -p rwxa -k docker
-w /usr/bin/dockerd -p rwxa -k docker
-w /etc/docker/daemon.json -p rwxa -k docker
```

Problem, solved.
