# The Unicorn Project book

![unicorn-book](https://images-na.ssl-images-amazon.com/images/I/51A4T36jisL._SX334_BO1,204,203,200_.jpg)

I just finished reading this book and I feel it is important to document some of the awesomeness I found in it. For starters, the Five Ideals, the explanation on what the 3 Horizons Business Model is and also, some personal lessons learned, which I want to make sure I have available to share with others.

As someone that works in IT on DevOps-related tech and is also an agile facilitator part of the company's DevOps culture transformation, this was beyond entertaining and useful.

The book was written by Gene Kim. You can find more info on him on [his website](http://www.realgenekim.me/) or [Twitter](https://twitter.com/realgenekim).

You can find the book [in Amazon](https://www.amazon.com/Unicorn-Project-Developers-Disruption-Thriving-ebook/dp/B07QT9QR41)


## The Five Ideals

Through my experience working in the tech industry, I've found a lot of the problems described in this book. I even faced several of them while reading about them. Gene Kim was able to name the areas where tech leaders (and actually everyone) needs to work on, to make our companies thrive.

### First Ideal: Locality and Simplicity


### Second Ideal: Focus, Flow and Joy


### Third Ideal: Improvement of Daily Work


### Fourth Ideal: Psychological Safety

I have been working as a Scrum Master, Agile coach, trainer and facilitator for some years now. I have found this ideal is paramount if we want our teams to work effectively and grow.

When I'm working as an agile facilitator (I stopped liking the Scrum Master name, because I feel it is restrictive), I understand fully the relevance of this. We need team members to share their experiences and learn from each other. We need them to feel safe to do so. We need a blameless environment for this, so they can experiment too. If your team members are not experimenting because they are afraid of some repercussion or some reaction, we are definitely doing it wrong.

I have seen that working as a facilitator on teams, you do work on a psychological area. You need to know your team members, invest in those relationships, kick-start that desired psychological safety. You need to find ways as a leader to modify your team meetings to be inclusive, for example knowing people manage attention differently, introverts and extroverts act differently and share info differently... 

### Fifth Ideal: Customer Focus

Customer Focus, customer obsession... This can't be achieved by just stating it as a value or as a goal for a team or even an organization. Teams need to understand their customers and care for them. This can be achieved probably in several ways. I have seen having team members meet with the customers, walk through their daily tasks (Gemba, anyone?), see what their painpoints are, that is actually helpful. Team members and customers will build a relationship and a personal connection to what will provide real value. Customers will feel the teams are really considering their needs, will be grateful for the time and effort. Teams will be able to attack real customer issues and provide real value, and since there was a personal connection there, they will feel joy doing so. This results in happy customers and happy teams :)

## Three Horizons Business Model

More info on McKinsey's Three Horizons can be found [here](https://www.mckinsey.com/business-functions/strategy-and-corporate-finance/our-insights/enduring-ideas-the-three-horizons-of-growth)


### Horizon 1
Core businesses that are stable, predictable and highly bureaucratic

### Horizon 2

Small businesses that generate new customers, capabilities or markets

### Horizon 3

Innovative orgs that explore risky and disruptive ideas that will generate the next Horizon 1/2 businesses

## Personal Lessons Learned (WIP)

### Blameless Post-mortems

Having a post-mortem after a release is much needed. But they should have certain characteristics to work and enable the Five Ideals further:
   - Blameless: no one is guilty for mistakes, no shaming other team members
   - Learning tools for improvement
   - Open to external people: when a team excels at this kind of activity, inviting external people to experience it is a great way to role-model wanted behaviors
   
I would say, these how effective retrospectives should look like (More info on How to lead effective retrospectives [here](#L68))

### Technical debt reduction work only

Sometimes, it is time for focusing on technical debt reduction work only. Having feature freeze time needs to happen from time to time to accomplish this. Real value resulting from it will be evident in the long run.

## Resources:

Other must reads:
   - [The Phoenix Project by Gene Kim, Kevin Behr and George Spafford](https://www.amazon.com/Phoenix-Project-DevOps-Helping-Business/dp/1942788290)
   - [The DevOps Handbook by Gene Kim, Patrick Debois, John Willis, Jez Humble and John Allspaw](https://www.amazon.com/DevOps-Handbook-World-Class-Reliability-Organizations/dp/1942788002)
   - [Agile Retrospectives: Making Good Teams Great by Esther Derby and Diana Larsen](https://www.amazon.com/Agile-Retrospectives-Making-Teams-Great/dp/0977616649)