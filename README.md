## Whoami

- Name: Irina Calvo
- Computer Science Engineer
- Agile facilitator
- DevOps Engineer
- Based in Costa Rica (that's in Central America)
- I enjoy training people on agile mindset
- I enjoy helping people accomplish cool techy stuff in their teams
- I enjoy using DevOps tools to do it
- Irina <3 containers

## Purpose

I'm uploading here interesting stuff I've worked on and useful snippets that help me get my job done.

My dream job is a mix of agile facilitator tasks that help towards culture transformation at my workplace and technical work towards DevOps implementation.
Having all of these components on my day-to-day makes my work life happy. 

Needed to add this here so I can remind myself from time to time why I do what I do.

## Contents

Still have a small number of files here, so no need for a real table of contents with links...

But I will be posting cool stuff I troubleshoot and want to remember, impressions on books I read, something similar to blog posts of stuff I'm thinking... GitBlog posts(?)


## FAQ

- Q: Irina, why are you not using a real blog for all of this stuff?
- A: I don't feel comfortable blogging, but I feel comfortable sharing stuff through Git and editing Markdown. I'm an engineer, not an influencer :P

### Snippets

Code Snippets can be found [here](https://gitlab.com/anairinac/daily-random-stuff/snippets)

